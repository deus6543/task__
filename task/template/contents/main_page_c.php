<?php
?>
<div class="control">
    <div class="row">
 
        <div class="col-sm">
        <form id="control_form">
        <div class="form-group float-right">
    <label for="control_sort">Сортировка</label>
    <select id="control_sort" class="form-control" name="control[sort]">     
    <option value="insert_date">Нет</option>        
    <option value="status">Статус</option>
            <option value="email">Email</option>
            <option value="name" >Имя</option>
        </select>
    
  </div>
 
        
    
  </div>
  <div class="col-2">
        <form id="control_form">
        <div class="form-group float-right">
    <label for="control_order">Порядок</label>
    <select id="control_order" class="form-control" name="control[order]">     
            <option value="ASC">Возрастание </option>
            <option value="DESC" >Убывание</option>
        </select>
</div>
</form>
    </div>
    </div>
</div>
<?php $model=new model;
if(!isset($_GET['page']))
{
    $offset=0;
    $start=1;
    $_GET['page']=1;
}
else{
    $offset=($_GET['page']-1)*3;
    $start=$_GET['page'];
}

if(!isset($_GET['control']['sort']))
{
    $_GET['control']['sort']="insert_date";
}
if(!isset($_GET['control']['order']))
{
    $_GET['control']['order']="ASC";
}



$data=$model->get_data(3,$offset,$_GET['control']['sort'],$_GET['control']['order']);

?>
<div class="row">
<div class="col-sm">
<div class="table_task">
<?php 
 $url=preg_replace('/&page\=\d+/',"",$_SERVER['REQUEST_URI']);
 if(!strpos($url,"?"))
 {
    $url.="?";
 }

 if(@$_SESSION['role']=="admin")
 {
    $write=true;
    $write_status=true;
 }
 else
 {
    $write=false;
    $write_status=false;
 }

foreach($data['data'] as $d)
{
    
    $name=$d['name'];
    $email=$d['email'];
    $status=$d['status'];
    $text=$d['text'];
    $id=$d['id'];
    $edit=$d['edit'];
    $method="update_task";
    ?>
    <form class="fm_task" method="POST">
    <?php
    include "task/task.php";
    ?>
    </form>
    <?php 
}  ?>
</div>
</div>

</div>

<div class="row">
    <div class="col-sm">
<nav >

  <ul class="pagination justify-content-center">
      <?php if($start>1): 
        ?>
  <li class="page-item"><a class="page-link" href="<?php echo  $url .'&page='.($_GET['page']-1); ?>">Пред</a></li>
  <?php endif ?>
  <?php 

  
  if($start<=2)
  {
    $start=1; 
  }
  else{
    $start-=1;
  }

    for($i=0; $i<3;$i++): 
    ?>
 
        
    <li id="<?php echo ($start+$i) ?>" class="page-item"><a class="page-link" href="<?php echo  $url .'&page='.($start+$i); ?>"><?php echo $start+$i; ?></a></li>
        <?php 
         if(($start+$i)>=ceil($data['count']/3))
         {
         break;
         }
         ?>
         
        <?php
        endfor ?>
        <?php if($start<ceil($data['count']/3)-1): ?>
  <li    class="page-item"><a  class="page-link" href="<?php echo  $url .'&page='.($_GET['page']+1); ?>">След</a></li>
  <li    class="page-item"><a  class="page-link" href="<?php echo  $url .'&page='.(ceil($data['count']/3)); ?>">Посл</a></li>
  <?php endif ?>
  
  </ul>
</nav>
</div>
  </div>



<script>

    $("#control_sort , #control_order").change(function(){
        console.log("12");
        $("#control_form").submit();
    });
    $(".page-item").removeClass("active");
    console.log( $(".page-item#<?php echo $_GET['page'] ?>"));
    $("li#<?php echo $_GET['page'] ?>.page-item").addClass("active");
    <?php  if(isset($_GET['control'])):?>
        
        $("#control_sort option[value='<?php echo $_GET['control']['sort'] ?>']").attr("selected","true");
        $("#control_order option[value='<?php echo $_GET['control']['order'] ?>']").attr("selected","true");
        <?php endif?>

        $("#email , #name , #task_text, #status").blur(function(){
            data={};
            len=$(this).parent().parent().parent().find("[name]").length;
            $(this).parent().parent().parent().find("[name]").each(function(i,e){
                console.log($(e));
                data[$(e).attr("name")]=$(e).val();
                console.log(data);
                if(i==len-1)
                {
                    $.post("/main_page",data,function(res){
                    if(res=="403")
                    {
                        alert("нужна авторизация");
                    }
                        location.reload();
                })
                }
                
            })
        })
    

</script>