<?php
class model{

    private  $mysqli;
    function __construct(){
        $this->mysqli = new bd_mysql();;
    }
    public function get_data($limit,$offset,$sort)
    {
        $data=array();
        $data['data']= $this->mysqli->get_results("SELECT * FROM data_task  ORDER BY ".$sort." ASC LIMIT ".$limit."  OFFSET ".$offset);
        $data['data']=$data['data'];
        $data['count']= $this->mysqli->get_row("select count(*) FROM data_task")['count(*)'];
        return $data;
    }
}