<?php
//маршрутизация запросов 
class route {
    static public $page = 'page';
    static function start() {
        //если смотрим главную страницу
        if ( isset( $_SERVER['REDIRECT_URL'] ) ) {
            $command = explode( '/', $_SERVER['REDIRECT_URL'] )[1];
            if ( isset( $_SERVER['REDIRECT_QUERY_STRING'] ) ) {
                $query = $_SERVER['REDIRECT_QUERY_STRING'];
                parse_str( $query, $_GET );
            }
            //если нет страницы то показываем 404
            if ( !file_exists( route::$page.'/'.$command.'.php' ) ) {
                $command = '404';
            }
            //показываем нужну странницу
            require_once( route::$page.'/'.$command.'.php' );
        } else {
            require_once( route::$page.'/main_page.php' );
        }
    }
  
    

}