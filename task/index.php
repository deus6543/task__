<?php
session_start();
//session_destroy();

define("ROOT_CORE","core");
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

require_once(ROOT_CORE."/route.php");
require_once(ROOT_CORE."/model.php");
require_once(ROOT_CORE."/view.php");
require_once(ROOT_CORE."/controller.php");
require_once(ROOT_CORE."/mysql.php");
//маршрутизация
route::start();
